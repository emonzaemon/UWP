﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItemBrowser_PAMO.Models;
using Template10.Services.NavigationService;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Template10.Mvvm;
using Windows.UI.Xaml.Navigation;
using ItemBrowser_PAMO.Services;


namespace ItemBrowser_PAMO.ViewModels
{
    class ItemPageViewModel : ViewModelBase
    {
        public ItemResponse ItemResponse { get; set; }

        private List<Item> _ItemList = new List<Item> { };
        public List<Item> ItemList { get { return _ItemList; } set { Set(ref _ItemList, value); } }


        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            try
            {
                ItemResponse = await ItemService.GetItem();
                ItemList = ItemResponse.item;
                await Task.CompletedTask;
            }
            catch (NullReferenceException e) { }
        }

    }
}
