﻿using System;
using System.Threading.Tasks;
using ItemBrowser_PAMO.Models;



namespace ItemBrowser_PAMO.Services
{
    public class ItemService : GlobalService
    {
        public async static Task<ItemResponse> GetItem()
        {
            string url = "https://eun1.api.riotgames.com/lol/static-data/v3/items?locale=pl_PL&tags=gold&api_key=RGAPI-d9d134fe-1807-4def-a4ef-86485ef1a34d";
            return await MakeRequest<ItemResponse>(url);
        }
    }
}

