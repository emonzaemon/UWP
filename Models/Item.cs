﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ItemBrowser_PAMO.Models
{
    [DataContract]
    public class Item
    {
        [DataMember]
        public string plaintext { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public int gold { get; set; }
    }
    [DataContract]
    public class ItemResponse
    {
        [DataMember]
        public List<Item> item { get; set; }
    }

}
